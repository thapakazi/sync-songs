package main

import (
	"flag"
	"fmt"
	"os"
)

var playlist_id string

func init() {
	flag.StringVar(&playlist_id, "playlist_id", "PLAM59Y34eXoujYt0tL5h6I3doo0WCL0YJ", "your playlist_id or url")
	flag.StringVar(&playlist_id, "p", "PLAM59Y34eXoujYt0tL5h6I3doo0WCL0YJ", "short hand for playlist_id")
	flag.Parse()

	setEnvIfMissing("YOUTUBE_DL_BIN", "/usr/bin/youtube-dl")
	setEnvIfMissing("EXTRACT_AUDIO", "y")
	setEnvIfMissing("DOWNLOAD_TO", "/tmp")
	setEnvIfMissing("ADB_BIN", "/usr/bin/adb")

}

func main() {

	fmt.Printf("\nfetching urls in: %v", playlist_id)
	playlist := getPlaylist(playlist_id)

	c := make(chan Response, len(playlist.Entries))
	playlist.download(c)
	playlist.printResults(c)

	v, ok := os.LookupEnv("SYNC_TO_PHONE")
	if ok && (v == "y" || v == "yes" || v == "true" || v == "t") {
		b := make(chan bool)
		playlist.syncMedia(b)
	}
}
