# source necessary env vars
include .env
export $(shell sed 's/=.*//' .env)
BINARY_NAME=sync_media

download:
	go run *.go -p=$(id)

build:
	go build -o bin/${BINARY_NAME}

install: build
	@cp -v bin/${BINARY_NAME} ${GOPATH}/bin/
