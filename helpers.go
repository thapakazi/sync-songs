package main

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

// simple handy function found on web, thanks i forgot your name
func printCommand(cmd *exec.Cmd) {
	if isDebugTrue() {
		fmt.Printf("==> Executing: %s\n", strings.Join(cmd.Args, " "))
	}
}

func printError(err error) {
	if err != nil && isDebugTrue() {
		os.Stderr.WriteString(fmt.Sprintf("==> Error: %s\n", err.Error()))
	}
}

func printOutput(outs []byte) {
	if len(outs) > 0 && isDebugTrue() {
		fmt.Printf("==> Output: %s\n", string(outs))
	}
}

func exec_cmd(binary string, args ...string) ([]byte, error) {
	cmd := exec.Command(binary, args...)
	printCommand(cmd)
	output, err := cmd.CombinedOutput()
	if err != nil {
		printError(err)
		return []byte{}, err
	}
	printOutput(output)
	return output, nil
}

func isDebugTrue() bool {
	if debug := os.Getenv("DEBUG"); debug == "y" || debug == "yes" || debug == "true" || debug == "True" {
		return true
	}
	return false
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func setEnvIfMissing(key string, defaultValue string) {
	value := getEnv(key, defaultValue)
	os.Setenv(key, value)
	if isDebugTrue() {
		fmt.Printf("\n%v : %v", key, defaultValue)
	}
}
