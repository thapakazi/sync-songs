package main

import (
	"fmt"
	"os"
	"path/filepath"
)

type Url struct {
	ID    string `json:"id"`
	Title string `json:"title"`
}

type Urls []Url

type Response struct {
	Status bool
	Msg    string
}

type Playlist struct {
	Type    string `json:"_type"`
	Entries Urls   `json:"entries"`
	ID      string `json:"id"`
	Title   string `json:"title"`
}

func (p *Playlist) download(c chan Response) {
	for _, u := range p.Entries {
		go downloadMedia(u, p.Title, c)
	}
}

func (p *Playlist) printResults(c chan Response) {
	var failedOnes []Response
	for i, _ := range p.Entries {
		response := <-c
		switch response.Status {
		case false:
			failedOnes = append(failedOnes, response)
		case true:
			fmt.Printf("\n %v. %v", i+1, response.Msg)
		}
	}

	if len(failedOnes) > 0 {
		fmt.Println("\nfailed downloads :(")
		for _, v := range failedOnes {
			fmt.Printf("\n %v. %v", "✖", v.Msg)
		}
	}
}

func (p *Playlist) syncMedia(c chan bool) {
	srcDir := filepath.Join(os.Getenv("DOWNLOAD_TO"), "audio", p.Title)
	setEnvIfMissing("SDCARD_BASE", "/sdcard")
	sdCardBase := os.Getenv("SDCARD_BASE")
	remoteDir := filepath.Join(sdCardBase, "listen")
	go sync2Remote(srcDir, remoteDir, c)
	if <-c {
		fmt.Printf("\n done sycning ***%v*** to mobile: %v", p.Title, remoteDir)
	}
}
