package main

const (
	FLAT_PLAYLIST       = "--flat-playlist"
	EXTRACT_AUDIO       = "-x"
	SIMULATE            = "-s"
	QUIET               = "-q"
	DUMP_SINGLE_JSON    = "-J"
	AUDIO_FORMAT_MP3    = "--audio-format=mp3"
	OUTPUT_FORMAT_AUDIO = "-oaudio/%(title)s-%(id)s.%(ext)s"
	OUTPUT_FORMAT       = "%(title)s-%(id)s.%(ext)s"
)
