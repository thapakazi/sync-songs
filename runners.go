package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
)

// sync2Remote syncs the download to remote
// remote in my case is my cell phone :)
func sync2Remote(srcDir string, destDir string, c chan bool) {
	execME := []string{}
	adbBIN := os.Getenv("ADB_BIN")
	execME = append(execME, adbBIN)
	execME = append(execME, "push")
	execME = append(execME, srcDir)
	execME = append(execME, destDir)
	exec_cmd(execME[0], execME[1:]...)
	c <- true
}

// youtubeDL call exec_cmd for now, wrapping around youtube-dl with exec_cmd
// TODO: improve me later
func youtubeDL(downloadUrlWithOpts ...string) ([]byte, error) {
	download_signature := []string{os.Getenv("YOUTUBE_DL_BIN")}
	for _, opt := range downloadUrlWithOpts {
		download_signature = append(download_signature, opt)
	}
	return exec_cmd(download_signature[0], download_signature[1:]...)
}

// getUrlsFromPlaylist : uses youtube-dl to pull all urls
// Eg: youtube-dl -J PLAM59Y34eXoujYt0tL5h6I3doo0WCL0YJ to get a single dump
func getPlaylist(id string) Playlist {
	output, _ := youtubeDL(FLAT_PLAYLIST, DUMP_SINGLE_JSON, id)
	var playlist Playlist
	err := json.Unmarshal(output, &playlist)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf(" i.e %v videos in ***%v***\n", len(playlist.Entries), playlist.Title)
	return playlist
}

// downloadMedia adjust downloding flags and calls youtubeDl
func downloadMedia(u Url, savePath string, c chan Response) {

	// for _, u := range urls {
	if isDebugTrue() {
		fmt.Printf("\ndownloading %v ", u.ID)
	}
	downloadArgs := []string{QUIET}
	downloadUrl := "https://youtube.com/watch?v=" + u.ID
	downloadArgs = append(downloadArgs, downloadUrl)
	defaultMedia := "video"
	mediaType := defaultMedia

	dataDir := os.Getenv("DOWNLOAD_TO")

	if os.Getenv("EXTRACT_AUDIO") == "y" {
		downloadArgs = append(downloadArgs, EXTRACT_AUDIO)
		downloadArgs = append(downloadArgs, AUDIO_FORMAT_MP3)
		mediaType = "audio"
	}

	downloadDir := filepath.Join(dataDir, mediaType, savePath, OUTPUT_FORMAT)
	downloadDirOpts := "-o" + downloadDir
	downloadArgs = append(downloadArgs, downloadDirOpts)

	if os.Getenv("SIMULATE") == "y" {
		downloadArgs = append(downloadArgs, SIMULATE)
	}

	var success bool
	if _, err := youtubeDL(downloadArgs...); err == nil {
		success = true
	}
	c <- Response{success, u.Title}
}
